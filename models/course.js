var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var courseSchema = new Schema({
    name: { type: String },
    course_number: { type: String },
    professor: { type: mongoose.Schema.Types.ObjectId, ref: 'Professor' },

    //term: { type: String },
    //projects: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Project' }],
    students: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Student' }],
  }, {
    timestamps: true,
  });

var Courses = mongoose.model('Course', courseSchema);

module.exports = Courses;
