// Professor Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var ProfessorSchema = Schema({
    first_name: { type: String, unique: true, required: true, max: 50 },
    last_name: { type: String, unique: true, required: true, max: 50 },
    prof_email: { type: String, required: true, max: 50 },
    prof_password: { type: String, required: true, max: 20 },
  });

ProfessorSchema.pre('save', function (next) {
  var professor = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
        if (err) {
          return next(err);
        }

        bcrypt.hash(professor.prof_password, salt, function (err, hash) {
            if (err) {
              return next(err);
            }

            professor.prof_password = hash;
            next();
          });
      });
  } else {
    return next();
  }
});

ProfessorSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.prof_password, function (err, isMatch) {
        if (err) {
          return cb(err);
        }

        cb(null, isMatch);
      });
  };

module.exports = mongoose.model('Professor', ProfessorSchema);
