var JwtStrategy = require('passport-jwt').Strategy;

// load up the user model
var Student = require('../models/student');
var Professor = require('../models/professor');
var config = require('../config/database'); // get db config file

module.exports = function (passport) {
  var opts = {};
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
    Student.findOne({ id: jwt_payload.id }, function (err, student) {
          if (err) {
            return done(err, false);
          }

          if (student) {
            done(null, student);
          } else {
            done(null, false);
          }
        });
  }));
};

module.exports = function (passport) {
  var opts = {};
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
      Professor.findOne({ id: jwt_payload.id }, function (err, professor) {
        if (err) {
          return done(err, false);
        }

        if (professor) {
          done(null, professor);
        } else {
          done(null, false);
        }
      });
    }));
};
