/* app module */

var serApp = angular.module('serApp', ['ngRoute', 'serControllers', 'serServices']);

serApp.config(['$routeProvider', '$locationProvider', function ($routeProvider,
$locationProvider) {
  $routeProvider.
  when('/', {
    templateUrl: 'partials/login.html',
    controller: 'loginCtrl',
  }).
  when('/register', {
    templateUrl: 'partials/register.html',
    controller: 'registerCtrl',
  })

  // Professor side of site
  .when('/professor', {
    templateUrl: 'partials/main_professor.html',
    controller: 'profCtrl',
  })
  .when('/professor/course', {
    templateUrl: 'partials/professor_add_course.html',
    controller: 'profCtrl',
  })
  .when('/professor/course/:courseId', {
    templateUrl: 'partials/professor_course_view.html',
    controller: 'courseCtrl',
  })
  .when('/professor/course/:courseId/addProject', {
    templateUrl: 'partials/professor_add_project.html',
    controller: 'courseCtrl',
  })
  .when('/professor/course/:courseId/addStudent', {
    templateUrl: 'partials/professor_add_student.html',
    controller: 'courseCtrl',
  })
  .when('/professor/course/:courseId/addTeam', {
    templateUrl: 'partials/professor_add_team.html',
    controller: 'courseCtrl',
  })

  //Student side of the website
  .when('/student', {
    templateUrl: 'partials/main_student.html',
    controller: 'studentCtrl',
  })
  .when('/student/course/:courseId', {
    templateUrl: 'partials/student_course_view.html',
    controller: 'courseCtrl',
  });
  $locationProvider.html5Mode(false).hashPrefix('!');

},
]);
