/*

 Controllers

*/

// Main controller module
var serControllers = angular.module('serControllers', []);

// login controller
serControllers.controller('loginCtrl', ['$scope', '$location', 'AuthService', '$window',
 function loginCtrl($scope, $location, AuthService, $window) {

  //student login

  $scope.student = {
    student_email: '',
    student_password: '',
  };

  $scope.studentSignIn = function () {
    AuthService.studentLogin($scope.student).then(function (msg) {
      $location.path('/student');
    },

    function (errMsg) {
      var alertPop = $window.alert('Login Failed Try Again!!');
    });
  };

  // profesor Login

  $scope.professor = {
    prof_email: '',
    prof_password: '',
  };

  $scope.professorSignIn = function () {
    AuthService.professorLogin($scope.professor).then(function (msg) {
      $location.path('/professor');
    },

    function (errMsg) {
      var alertPop = $window.alert('Login Failed Try Again!!');
    });
  };

},
]);

// registration controller
serControllers.controller('registerCtrl', ['$scope', '$location', 'AuthService', '$window',
 function registerCtrl($scope, $location, AuthService, $window) {

  //professor signup

  $scope.professor = {
    first_name: '',
    last_name: '',
    prof_email: '',
    prof_password: '',
    profConfirmPassword: '',
  };
  $scope.professorSignup = function () {
    AuthService.professorRegister($scope.professor).then(function (msg) {
      $location.path('/');
      var alertPop = $window.alert('Professor Registration success!');

    }, function (err) {

        var alertPop = $window.alert('Professor Registration failed!');
      });
  };

  // student studentSignup

  $scope.student = {
    first_name: '',
    last_name: '',
    student_email: '',
    student_password: '',
    confirmPassword: '',

  };
  $scope.studentSignup = function () {
    AuthService.studentRegister($scope.student).then(function (msg) {
      $location.path('/');
      var alertPop = $window.alert('Student Registration Success!');

    }, function (err) {

        var alertPop = $window.alert('Student Registration Failed!');
      });
  };
},
]);

// student controller
serControllers.controller('studentCtrl', ['$scope', '$location', 'AuthService', 'CourseData',
function studentCtrl($scope, $location, AuthService, CourseData) {

  getCourses();

  //student logout
  $scope.destroyStudentSession = function () {
    AuthService.logout();
    $location.path('/');
  };

  // get all courses from database
  function getCourses() {
    CourseData.getCourses().then(function (res) {
      $scope.courses = res.data;
    },

    function (err) {
      $scope.status = 'Unable to load courses: ' + err.message;
    });
  }

  $scope.courseList = [{
    course_name: 'Software Dev',
    course_num: 'SER 310',
  },
{
    course_name: 'Calc',
    course_num: 'MA 141',
  },
 ];
  $scope.editing = false;
  $scope.addCourse = function (course) {
    $scope.courses.push(course);
    $scope.schedule = {};
  };

  $scope.suggestedSchedule = [];
  $scope.addToSchedule = function (course) {
    $scope.suggestedSchedule.push(course);
  };

  $scope.removeFromSchedule = function (course) {
    $scope.suggestedSchedule.pop(course);
  };
},
]);

serControllers.controller('profCtrl', ['$scope', '$location',
 'AuthService', '$http', 'CourseData', '$window', '$routeParams', 'CourseView',
 function profCtrl($scope, $location, AuthService, $http, CourseData,
  $window, $routeParams, CourseView) {

  $scope.course = {
    name: '',
    course_number: '',
    profId: '',
  };
  getCourses();

  //create course

  $scope.addCourse = function () {
    AuthService.addCourse($scope.course).then(function (msg) {
      $location.path('/professor');
      var alertPop = $window.alert('Course add success!');

    }, function (err) {

        var alertPop = $window.alert('Course add failed!');
      });
  };

  // get all courses from database
  function getCourses() {
    CourseData.getCourses().then(function (res) {
      $scope.courses = res.data;
    },

    function (err) {
      $scope.status = 'Unable to load courses: ' + err.message;
    });
  }

  //professor logout

  $scope.destroyProfessorSession = function () {
    AuthService.logout();
    $location.path('/');
  };

},
]);

serControllers.controller('courseCtrl', ['$scope', 'CourseView', '$routeParams',
'StudentData', 'StudentAction',  function ($scope, CourseView, $routeParams,
   StudentData, StudentAction) {
    $scope.addStudent = addStudent;
    $scope.courseId = $routeParams.courseId;

    setup();
    getStudents();

    function setup() {

      CourseView.getCourseById($routeParams.courseId).then(function (res) {
        $scope.course = res;
      });

    }

    // get all Students from database
    function getStudents() {
      StudentData.getStudents().then(function (res) {
        $scope.students = res.data;
      },

      function (err) {
        $scope.status = 'Unable to load students: ' + err.message;
      });
    }

    //add student to course

    function addStudent(studentId) {
      StudentAction.addStudentToCourse(studentId, $routeParams.courseId).then(function (res) {

      });
    }

  },
]);
