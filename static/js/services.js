var serServices = angular.module('serServices', ['ngResource']);

serServices.factory('AuthService', ['$q', '$http',
 function ($q, $http) {
  var LOCAL_TOKEN_KEY = 'heykidhowareya';
  var isAuthenticated = false;
  var authToken;
  var userInfo;

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }

  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }

  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;

    // Set the token as header for your requests!
    $http.defaults.headers.common.Authorization = authToken;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    isAuthenticated = false;
    $http.defaults.headers.common.Authorization = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }

  // Registration

  var studentRegister = function (student) {
    return $q(function (resolve, reject) {
      $http.post('/student/signup', student).then(function (result) {
        if (result.data.success) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var professorRegister = function (professor) {
    return $q(function (resolve, reject) {
      $http.post('/professor/signup', professor).then(function (result) {
        if (result.data.success) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  // Login and authenitcation

  var studentLogin = function (student) {
    return $q(function (resolve, reject) {
      $http.post('/student/authenticate', student).then(function (result) {
        if (result.data.success) {
          storeUserCredentials(result.data.token);

          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var professorLogin = function (professor) {
    return $q(function (resolve, reject) {
      $http.post('/professor/authenticate', professor).then(function (result) {
        if (result.data.success) {
          storeUserCredentials(result.data.token);
          console.log(result.data.msg);
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var addCourse = function (course) {
    return $q(function (resolve, reject) {
      $http.post('/course/addCourse', course).then(function (result) {
        if (result.data.success) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var logout = function () {
    destroyUserCredentials();
  };

  return {
    studentRegister: studentRegister,
    professorRegister: professorRegister,
    studentLogin: studentLogin,
    professorLogin: professorLogin,
    addCourse: addCourse,
    logout: logout,
  };
},
]);

serServices.factory('CourseView', ['$http', function ($http) {

      function getCourseById(id) {
        return $http.get('http://localhost:3000/course/' + id).then(function (res) {
          return res.data;
        }, function (err) {

            console.log(err);
          });
      }

      return {
        getCourseById: getCourseById,
      };

    },

]);

serServices.factory('CourseData', ['$http', function ($http) {
  var courseData = {};

  courseData.getCourses = function () {
    return $http.get('/course/');
  };

  return courseData;

},

]);

serServices.factory('StudentData', ['$http', function ($http) {
  var studentData = {};

  studentData.getStudents = function () {
    return $http.get('/student/');
  };

  return studentData;

},

]);

serServices.factory('StudentAction', ['$http', function ($http) {
  function addStudentToCourse(studentId, courseId) {
    return $http({
      method: 'POST',
      url: 'http://localhost:3000/courses/' + courseId + '/student',
      data: {
        studentId: studentId,
      },
    }).then(function (res) {
      return res.data;
    });
  }

  return {
    addStudentToCourse: addStudentToCourse,
  };
},
]);
