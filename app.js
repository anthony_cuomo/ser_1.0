var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');
var path = require('path');
var cookieParser = require('cookie-parser');
var jwt = require('jwt-simple');
var config = require('./config/database');
var student = require('./models/student');
var professor = require('./models/professor');
var course = require('./models/course');
var professorRouter = require('./routes/professorRouter');
var studentRouter = require('./routes/studentRouter');
var courseRouter = require('./routes/courseRouter');

//Connect to the Mongo DB
mongoose.connect(config.database);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('Connected Correctly to Database Server');
});

// passport
require('./config/passport')(passport);

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/', express.static(path.join(__dirname, './static')));
app.use('/professor', professorRouter);
app.use('/student', studentRouter);
app.use('/course', courseRouter);

module.exports = app;
