var express = require('express');
var studentRouter = express.Router();
var bodyParser  = require('body-parser');
var mongoose = require('mongoose');
var Student = require('../models/student');
var passport	= require('passport');
var config = require('../config/database'); // get db config file
var jwt = require('jwt-simple');
var Verify = require('./verify');



/* Retrieve All Students */
studentRouter.get('/', function (req, res) {
  Student.find({}, function (err, students) {
    if (err) throw err;
    res.json(students);

    res.end('retrieve all students');
  });

});

/* Register Student */
studentRouter.post('/signup', function (req, res) {
  if (!req.body.first_name || !req.body.student_password) {
    res.json({ success: false, msg: 'Please pass name and password.' });
  } else {
    var newStudent = new Student({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      student_email: req.body.student_email,
      student_password: req.body.student_password,
    });

    // save the student
    newStudent.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({ success: false, msg: 'Student already exists.' });
      }

      res.json({ success: true, msg: 'Successful created new student.' });
    });
  }
});

// route to authenticate a student post student/authenticate
studentRouter.post('/authenticate', function (req, res) {
  Student.findOne({
    student_email: req.body.student_email,
  }, function (err, student) {
      console.log(student);
      if (err) throw err;

      if (!student) {
        res.send({ success: false, msg: 'Authentication failed. student not found' });

      } else {
        // check if password matches
        student.comparePassword(req.body.student_password, function (err, isMatch) {
          if (isMatch  && !err) {

            // if user is found and password is right create token
            var token = jwt.encode(student, config.secret);

            // return the info including token as json
            res.json({ success: true, token: 'JWT ' + token, id: student._id });
          } else {
            res.send({ success: false, msg: 'Authentication failed. Wrong password' });
          }
        });
      }
    });
});

/* login student to homepage
route to student homepage /student/studenthome */
studentRouter.get('/studenthome', passport.authenticate('jwt', { session: false }),
function (req, res) {
  var token = Verify.getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    Student.findOne({
      first_name: decoded.first_name,
    }, function (err, student) {
      if (err) throw err;
      if (!student) {
        return res.status(403).send({ success: false,
          msg: 'Authentication failed. User not found', });
      }else {
        res.json({ success: true, msg: 'Welcome in the member area ' + student.first_name + '!' });
      }
    });

  }else {
    return res.status(403).send({ success: false, msg: 'No Token provided' });
  }
});

module.exports = studentRouter;
