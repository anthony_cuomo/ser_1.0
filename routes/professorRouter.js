var express = require('express');
var professorRouter = express.Router();
var bodyParser  = require('body-parser');
var mongoose = require('mongoose');
var Professor = require('../models/professor');
var passport	= require('passport');
var config = require('../config/database'); // get db config file
var jwt = require('jwt-simple');
var Passport = require('../config/passport');
var Verify = require('./verify');

/* get all advisors for testing */
professorRouter.get('/', function (req, res) {
  Professor.find({}, function (err, advisors) {
    if (err) throw err;
    res.json(advisors);

    res.end('retrieve all profesors');
  });

});

/* Register Advisor create a new user account /advisor/signup */
professorRouter.post('/signup', function (req, res) {
  if (!req.body.first_name || !req.body.prof_password) {
    res.json({ success: false, msg: 'Please pass name and password.' });
  } else {
    var newProfessor = new Professor({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      prof_email: req.body.prof_email,
      prof_password: req.body.prof_password,
    });

    // save the advisor
    newProfessor.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({ success: false, msg: 'Username already exists.' });
      }

      res.json({ success: true, msg: 'Successful created new professor.' });
    });
  }
});

// route to authenticate a advisor post advisor/authenticate
professorRouter.post('/authenticate', function (req, res) {
  Professor.findOne({
    prof_email: req.body.prof_email,
  }, function (err, professor) {
      console.log(professor);
      if (err) throw err;

      if (!professor) {
        res.send({ success: false, msg: 'Authentication failed. User not found' });

      } else {
        // check if password matches
        professor.comparePassword(req.body.prof_password, function (err, isMatch) {
          if (isMatch && !err) {

            // if user is found and password is right create token
            var token = jwt.encode(professor, config.secret);

            // return the info including token as json
            res.json({ success: true, token: 'JWT ' + token });
          }else {
            res.send({ success: false, msg: 'Authentication failed. Wrong password' });
          }
        });
      }
    });
});


/* login Professor
 route to a advisor home /professor/professorhome */

professorRouter.get('/professorhome', passport.authenticate('jwt', { session: false }),
function (req, res) {
  var token = Verify.getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    Professor.findOne({
      first_name: decoded.first_name,
    }, function (err, professor) {
      if (err) throw err;
      if (!professor) {
        return res.status(403).send({ success: false,
          msg: 'Authentication failed. User not found', });
      }else {
        res.json({ success: true, msg: 'Welcome in the member area ' + professor.first_name + '!' });
      }
    });

  }else {
    return res.status(403).send({ success: false, msg: 'No Token provided' });
  }
});

module.exports = professorRouter;
