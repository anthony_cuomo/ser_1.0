var express = require('express');
var courseRouter = express.Router();
var bodyParser  = require('body-parser');
var mongoose = require('mongoose');
var Professor = require('../models/professor');
var Student = require('../models/student');
var Course = require('../models/course');
var passport	= require('passport');
var config = require('../config/database'); // get db config file
var jwt = require('jwt-simple');
var Passport = require('../config/passport');
var Verify = require('./verify');

/* get all advisors for testing */
courseRouter.get('/', function (req, res) {
  Course.find({}, function (err, courses) {
    if (err) throw err;
    res.json(courses);

    res.end('retrieve all courses');
  });

});

/* create a new course for mainProf account /course/addCourse */
courseRouter.post('/addCourse', function (req, res) {
  if (!req.body.name || !req.body.course_number) {
    res.json({ success: false, msg: 'Please pass course info.' });
  } else {
    var newCourse = new Course({
      name: req.body.name,
      course_number: req.body.course_number,
    });

    // save the advisor
    newCourse.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({ success: false, msg: 'course already exists.' });
      }

      res.json({ success: true, msg: 'Successful created new course.' });
    });
  }
});

courseRouter.route('/:courseId')
  .get(function (req, res, next) {
    Course.findById(req.params.courseId, function (err, course) {
        if (err)
            throw err;
        res.json(course);

      });
  })
  .delete(function (req, res, next) {
    Course.findByIdAndRemove(req.params.courseId, function (err, resp) {
        if (err) throw err;
        res.json(resp);
      });
  });
// add students to course
courseRouter.route('/:courseId/student')
  .post(function (req, res, next) {
    Course.findById(req.params.courseId, function (err, course) {
        if (err) throw err;
        course.students.push(req.body.studentId);
        course.save(function (err, course) {
            res.json(course);
          });
      });
  });

module.exports = courseRouter;
